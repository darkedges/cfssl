#!/bin/sh


init_db() {
    # Create Database if it does not exist
    ls -l  /src/cfssl/certdb/mysql
    cat /src/cfssl/certdb/mysql/dbconf.yml
    goose -path /src/cfssl/certdb/mysql -env production up
}

refresh_ocsp() {
    echo "Refreshing OCSP Database"
    exec /usr/bin/cfssl ocsprefresh \
        -db-config "db-config.json" \
        -ca "${CA_PATH}/intermediate.pem" \
        -responder "${CA_PATH}/server-ocsp/server-ocsp.pem" \
        -responder-key "${CA_PATH}/server-ocsp/server-ocsp-key.pem"
}

start_signer() {
    # Run service
    exec /usr/bin/cfssl serve \
        -address=0.0.0.0 \
        -ca "${CA_PATH}/intermediate.pem" \
        -ca-key "${CA_PATH}/intermediate-key.pem" \
        -config "${CA_PATH}/cfssl-config.json" \
        -db-config "${CA_PATH}/db-config.json" \
        -port=8888 \
        -responder "${CA_PATH}/server-ocsp/server-ocsp.pem" \
        -responder-key "${CA_PATH}/server-ocsp/server-ocsp-key.pem"
}

start_ocsp() {
    # Run service
    exec /usr/bin/cfssl ocspserve  \
        -address=0.0.0.0 \
        -port=8889 \
        -db-config "${CA_PATH}/db-config.json"
}

CMD="${1-start_signer}"

echo "Command is $CMD"

case "$CMD" in
init_db)
    init_db
    ;;
start_signer)
    start_signer
    ;;
start_ocsp)
    start_ocsp
    ;;
refresh_ocsp)
    refresh_ocsp
    ;;
*)
    exec "$@"
esac