#!/bin/bash -eux

# Bundle up the variosu CAs
cat ../intermediate.pem ../root.pem > bundle.pem
# Execute the test
openssl ocsp \
    -CAfile bundle.pem \
    -issuer ../intermediate.pem \
    -no_nonce \
    -cert test.pem \
    -text \
    -url ${CFSSL_URL-http://10.0.0.37:8889}