#!/bin/bash -eux

# Create then request and execute against Signer
cn=*.darkedges.com
# openssl req -new -newkey rsa:2048 -nodes -out example.csr -keyout example.key -subj "/C=AU/ST=Victoria/L=Melbourne/O=DarkEdges/OU=IT/CN=$cn/emailAddress=admin@$cn"
export auth_key=0123456789ABCDEF0123456789ABCDEF
csr=$(cat "example.csr")
certificate_req=$(jq -n -c -j \
                    --arg csr  "${csr}" \
                    --arg cn   "${cn}" \
                    '{"certificate_request":($csr+"\n"),"profile":"intermediate"}')
base64_certificate_req=`echo ${certificate_req} | base64 | tr --delete '\n'`
hex_encoded_onboarding_key=`echo ${certificate_req} | openssl dgst -sha256 -binary -mac HMAC -macopt "hexkey:$auth_key" | base64`
auth_req=$(jq -n -c -j --arg token "${hex_encoded_onboarding_key}" --arg req "${base64_certificate_req}" '{"token":$token,"request":$req}')
echo "$auth_req" > authsign_request.json
curl -X POST -H "Content-Type: application/json" -d @authsign_request.json ${CFSSL_URL-http://10.0.0.37:8888}/api/v1/cfssl/authsign | jq -r '.result.certificate' > test.pem
openssl x509 -in test.pem -text -noout