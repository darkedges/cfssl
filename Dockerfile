FROM golang:1.12.5-alpine3.9

# Add cfssl
RUN apk update && \
    apk add --virtual build-dependencies git gcc libc-dev make && \
    export GO111MODULE=on && \
    mkdir -p $GOPATH/src/github.com/cloudflare/cfssl && \
    cd $GOPATH/src/github.com/cloudflare/cfssl && \
    git clone https://github.com/darkedges/cfssl . &&\
    git checkout darkedges/copyCSRExtensions && \
    go get github.com/GeertJohan/go.rice/rice && rice embed-go -i=./cli/serve && \
    mkdir -p bin && \ 
    cd bin && \
    go build ../cmd/cfssl && \
    mkdir -p $GOPATH/src/bitbucket.org/liamstask/goose && \
    cd $GOPATH/src/bitbucket.org/liamstask/goose && \
    git clone https://bitbucket.org/liamstask/goose.git . &&\
    mkdir -p bin && \ 
    cd bin && \
    go get ../cmd/goose && go build ../cmd/goose

FROM alpine:3.9

ENV TINI_VERSION=v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static-amd64 /tini
RUN chmod +x /tini

ENTRYPOINT [ "/tini","--" ]

# Run intermediate CA
ADD *.pem /home/cfssl/
ADD ssl/* /home/cfssl/ssl/
ADD server-ocsp/* /home/cfssl/server-ocsp/
ADD cfssl-config.json /home/cfssl
ADD db-config.json /home/cfssl
ADD files/docker-entrypoint.sh /

COPY --from=0 /go/src/github.com/cloudflare/cfssl/bin/ /usr/bin
COPY --from=0 /go/src/bitbucket.org/liamstask/goose/bin/ /usr/bin
COPY --from=0 /go/src/github.com/cloudflare/cfssl/certdb/mysql/ /src/cfssl/certdb/mysql

# Create cfssl user and template database
RUN addgroup -g 1000 -S cfssl && \
    adduser -u 1000 -S cfssl -G cfssl 

# CFSSL volume
ENV CA_PATH /home/cfssl

# CFSSL service port
EXPOSE 8888 8889

USER cfssl

WORKDIR /home/cfssl

CMD  ["/docker-entrypoint.sh"]
