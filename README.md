# Own CA

```bash
cfssl gencert -initca root.json | cfssljson -bare root -
cfssl gencert -ca root.pem -ca-key root-key.pem -config cfssl-config.json -profile intermediate intermediate.json | cfssljson -bare intermediate -
cd server-ocsp
cfssl gencert -ca ../intermediate.pem -ca-key ../intermediate-key.pem -config ../cfssl-config.json -profile ocsp ocsp.csr.json | cfssljson -bare server-ocsp -
cd ..
cfssl serve -address 10.0.0.30 -ca-key intermediate-key.pem -ca intermediate.pem -config cfssl-config.json -responder server-ocsp/server-ocsp.pem -responder-key server-ocsp/server-ocsp-key.pem -tls-key=ssl/key.pem -tls-cert=ssl/cert.pem
```

## build

```bash
docker build . -t darkedges/cfssl:1.0.0
docker run -p 8888:8888 -v /tmp:/db/sqlite darkedges/cfssl:1.0.0
docker ps | grep cfssl | awk '{print $1}' | xargs docker stop]
docker exec -it $(docker ps | grep cfssl | awk '{print $13}') /usr/bin/refreshOCSP.sh
```

## test

### Linux

```bash
./testAuthSign.sh
docker exec -it $(docker ps | grep cfssl | awk '{print $13}') /usr/bin/refreshOCSP.sh
./testOCSP.sh
```

### Windows

```bash
winpty docker exec -it $(docker ps | grep cfssl | awk '{print $13}') //usr/bin/refreshOCSP.sh
```

## Validate

```bash
openssl x509 -text -in intermediate.pem
curl --data @test.json https://pki.darkedges.com/api/v1/cfssl/sign:8888

openssl x509 -in test.crt -text -noout
openssl verify -CAfile root.pem -untrusted intermediate.pem test.crt
```

## Java

```bash
keytool -genkey -alias server -keyalg RSA -keysize 2048 -keystore keystore.jks
keytool -certreq -alias server -file darkedges.csr -keystore keystore.jks -keyalg RSA
curl -d @c:\development\verifymyid\cfssl\test.json -H "Content-Type: application/json" http://localhost:8080/api/v1/cfssl/sign
keytool -importcert --trustcacerts -keystore keystore.jks -file c:\development\verifymyid\cfssl\root.pem -alias darkedgesroot
keytool -importcert --trustcacerts -keystore keystore.jks -file c:\development\verifymyid\cfssl\intermediate.pem -alias darkedgesintermediate
keytool -import -alias server -file darkedges.crt -keystore keystore.jks
```

## Auth Sign

```bash
cfssl serve -config cfssl-config.json -db-config db-config.json -ca-key intermediate-key.pem -ca intermediate.pem -responder-key server-ocsp\server-ocsp-key.pem -responder server-ocsp\server-ocsp.pem -loglevel 0
cn=example.com
openssl req -new -newkey rsa:2048 -nodes -out example.csr -keyout example.key -subj "/C=AU/ST=Victoria/L=Melbourne/O=DarkEdges/OU=IT/CN=$cn/emailAddress=admin@$cn"
export auth_key=0123456789ABCDEF0123456789ABCDEF
csr=$(cat "example.csr")
certificate_req=$(jq -n -c -j \
                    --arg csr  "${csr}" \
                    --arg cn   "${cn}" \
                    '{"certificate_request":($csr+"\n"),"profile":"intermediate"}')
base64_certificate_req=`echo ${certificate_req} | base64 | tr --delete '\n'`
hex_encoded_onboarding_key=`echo ${certificate_req} | openssl dgst -sha256 -binary -mac HMAC -macopt "hexkey:$auth_key" | base64`
auth_req=$(jq -n -c -j --arg token "${hex_encoded_onboarding_key}" --arg req "${base64_certificate_req}" '{"token":$token,"request":$req}')
echo "$auth_req" > authsign_request.json
curl -X POST -H "Content-Type: application/json" -d @authsign_request.json http://localhost:8888/api/v1/cfssl/authsign | jq -r '.result.certificate' > test.crt
openssl x509 -in test.crt -text -noout
```
